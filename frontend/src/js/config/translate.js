import localesFr  from '../locales/fr';
import localesEn  from '../locales/en';
import '../../../node_modules/angular-i18n/angular-locale_fr';

export /* @ngInject */ function TranslateConfiguration($translateProvider) {
    $translateProvider.useMissingTranslationHandlerLog();
    $translateProvider.addInterpolation('$translateMessageFormatInterpolation');
    $translateProvider.preferredLanguage('fr');

    $translateProvider.translations('fr', localesFr);
}
