class FlashMessagesController {
    /* @ngInject */
    constructor(FlashMessages) {
        this.FlashMessages = FlashMessages;
    }

    pause() {
        this.FlashMessages.pause();
    }

    resume() {
        this.FlashMessages.resume();
    }

    remove(message) {
        this.FlashMessages.remove(message);
    }
}

export function FlashMessagesDirective() {
    return {
        restrict: 'E',
        scope: true,
        controller: FlashMessagesController,
        controllerAs: 'ctrl',
        template: `
            <div class="flash-messages" ng-mouseover="ctrl.pause()" ng-mouseleave="ctrl.resume()">
                <div ng-repeat="message in ctrl.FlashMessages.messages" class="flash-message {{ message.type }}"
                    ng-click="ctrl.remove(message)">
                    <span ng-switch on="message.type">
                        <i ng-switch-when="success" class="fa fa-check"></i>
                        <i ng-switch-when="info" class="fa fa-info"></i>
                        <i ng-switch-when="warning" class="fa fa-exclamation"></i>
                        <i ng-switch-when="error" class="fa fa-times"></i>
                    </span>
                    <span ng-if="message.trusted" ng-bind-html="message.message"></span>
                    <span ng-if="!message.trusted" ng-bind="message.message"></span>
                </div>
            </div>
        `
    };
}
