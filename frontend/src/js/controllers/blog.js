export class BlogController {
    /* @ngInject */
    constructor(posts, UserService) {
        this.posts = posts;
        this.tab = 'blog';
        this.username = UserService.getUser();
        this.post = {};
    }

    selectTab(setTab) {
        this.tab = setTab;
    }

    isSelected(checkTab) {
        return this.tab === checkTab;
    }

    static resolve() {
        return {
            posts: /* @ngInject */ ($http, PostsSerializer) => $http.get('posts.json')
                .then(response => response.data.map(post => PostsSerializer.deserializeModel(post)))
        }
    }
    static template() {
        return `
            <div class="container">
                <div class="row">
                    <h2>Bienvenu {{ ctrl.username }}</h2>
                    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    {{ctrl.username}}
                        <ul class="list-unstyled" ng-show="ctrl.isSelected('blog')">
                            <li ng-repeat="post in ctrl.posts track by $index" ng-click="ctrl.selectTab($index)" >
                                <h2>{{ post.title }}</h2>
                                <p class="synopsis">{{ post.body[0] | limitTo:100 }}...</p>
                                <span>Article publié par <strong>{{ post.author }}</strong></span>
                                <hr>
                            </li>
                        </ul>

                        <div class="post" ng-repeat="post in ctrl.posts track by $index" ng-show="ctrl.isSelected($index)">
                            <div class="single">
                                <h2>{{ post.title }}</h2>
                                <span>Par {{ post.author }}</span>
                                <div class="post-body">
                                    <p ng-repeat="paragraph in post.body">{{ paragraph }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
    }
}
